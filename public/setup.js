/**
 * Setup
 */

let GssSetup = (function () {

    const SENSOR_API_BASE_URL = 'http://81.169.187.7:8080/sta/v1.0';
    // const SENSOR_API_BASE_URL = 'http://localhost:8080/sta/v1.0';

    const fetchedSensorList = [];
    const fetchedObservationList = {};

    function init() {
        console.log('Loading module: GssSetup...done.');
    }

    /**
     * Loads the sensors.json file with the Fetch API
     * https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
     */
    function createSensors() {

        fetch('sensors.json')
            .then(function (response) {
                return response.json();
            })
            .then(function (result) {
                for (let i = 0; i < result.length; i++) {
                    addSensor(result[i]);
                }
            })
            .catch(function (error) {
                console.error('Error: ', error);
            });
    }

    /**
     * Loads the csv file with the Fetch API
     * ------------------------------------------------------------------
     * Parsing the content of the csv and created STA objects 
     * to be consumed by the STA endpoint <url>/Datastreams(id)/Observation
     * 
     * SensorUp provides a documentation of the STA which can be used 
     * to specify the objects for the Fraunhofer version
     * http://developers.sensorup.com/docs
     */
    function insertObservations() {

        var start = new Date(), end;

        const fileName = document.getElementById('csvFile').value;
        const datastreamId = document.getElementById('datastreamId').value;
        const notify = document.getElementById('import-notification');

        if (datastreamId === '') {
            notify.innerHTML('Supply a datastream id.');
            return;
        }

        if (datastreamId.replace(/[^0-9.]/g, "").length === 0) {
            return;
        }

        fetch('csv/' + fileName + '.csv')
            .then(function (response) {
                return response.blob();
            }).then(function (result) {
                console.log(result);
                var reader = new FileReader();
                reader.onload = function (progressEvent) {
                    let cry;
                    let count = 0;

                    var lines = this.result.split('\n');
                    for (var line = 0; line < lines.length; line++) {
                        let vals = lines[line].split(',');
                        let parseTime2 = new Date(vals[0]);
                        let resultTime = new Date();
                        cry = vals[0]

                        let rtime = new Date(cry).toISOString();

                        let tmsso = {
                            "phenomenonTime": rtime,
                            "resultTime": rtime,
                            "result": parseFloat(vals[1]),
                            "Datastream": { "@iot.id": 5 }
                        }

                        count++;

                        let headers = new Headers({
                            "Access-Control-Allow-Origin": "*",
                            "Content-Type": "application/json; charset=utf-8"
                        })

                        fetch(`${SENSOR_API_BASE_URL}/Datastreams(${datastreamId.replace(/[^0-9.]/g, "")})/Observations`, {
                            method: 'POST',
                            headers: headers,
                            mode: 'no-cors',
                            body: JSON.stringify(tmsso)
                        })
                            .then(function (response) {
                                // console.log(response);
                            });

                        if (count % 1000) {
                            console.log('processing...')
                        }

                        // Very simple log to calc the processing time
                        if (count === 8040) {
                            // return;
                            end = new Date();
                            let dur = end.getMilliseconds() - start.getMilliseconds();
                            console.log('Time: ' + dur / 1000);
                        }
                    }
                    console.log('count: ', count);
                };
                reader.readAsText(result);
            });
    }

    /**
     * Sends a POST request to the STA with the contents of a sensor object
     * @param {*} item 
     */
    function addSensor(item) {

        let headers = new Headers({
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json"
        });

        let options = {
            method: 'POST',
            headers: headers,
            mode: 'no-cors',
            body: JSON.stringify(item)
        }

        console.log(item);

        fetch(`${SENSOR_API_BASE_URL}/Things`, options)
            .then(function (result) {
                console.log('...success.');
            })
            .catch(function (error) {
                console.error('Problem: ', error);
                // return;
            });
    }

    /**
     * Sends a GET request to the STA and checks for the availability of the nextLink key.
     * The Fraunhofer STA implementations is limited to 20 results per request in the tomcat configuration.
     * The value was changes to 100 results per request.
     * location: <tomcat-location>\webapps\<war-file-name>\META-INF\context.xml 
     * @param {*} nextLink 
     */
    function getThings(nextLink) {

        console.log('...ping...');

        let part;
        if (nextLink) {
            part = nextLink.split('?');
        }

        let url = part ? `${SENSOR_API_BASE_URL}/Things?${part[1]}` : `${SENSOR_API_BASE_URL}/Things?$expand=Datastreams`;

        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (result) {
                fetchedSensorList.push.apply(fetchedSensorList, result.value);
                if (result['@iot.nextLink']) {
                    getThings(result["@iot.nextLink"])
                } else {
                    console.log('...done.');
                };
            })
            .catch(function (error) {
                console.error('Error: ', error);
            });
    }


    /**
     * Iterates through the list of available sensors and retrieves the contents of the datastream 
     */
    function loadDatastreams() {
        // 
        for (let i = 0; i < fetchedSensorList.length; i++) {
            console.log('Run: ' + i);
            let name = fetchedSensorList[i].name;
            let id = fetchedSensorList[i]['Datastreams'][0]['@iot.id'];
            fetchedObservationList[name] = [];
            getObservations(id, name);
        }
    }

    /**
     * Sends a GET request to the STA and checks for the availability of the nextLink key.
     * The Fraunhofer STA implementations is limited to 20 results per request in the tomcat configuration.
     * The value was changes to 100 results per request.
     * location: <tomcat-location>\webapps\<war-file-name>\META-INF\context.xml 
     * @param {*} nextLink 
     */
    function getObservations(datastreamId, name, nextLink) {
        let part;
        if (nextLink) {
            part = nextLink.split('?');
        }

        if (!datastreamId) {
            return;
        }

        var url = part ? `${SENSOR_API_BASE_URL}/Datastreams(${datastreamId})/Observations?${part[1]}` : `${SENSOR_API_BASE_URL}/Datastreams(${datastreamId})/Observations?$resultFormat=dataArray`;
       
        let headers = new Headers({
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json"
        });

        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (result) {
                fetchedObservationList[name].push.apply(fetchedObservationList[name], result.value[0].dataArray);
                if (result['@iot.nextLink']) getObservations(datastreamId, name, result["@iot.nextLink"]);
            })
            .catch(function (error) {
                console.error('Error: ', error);
            });

    }

    /**
     * Creates the CZML object from the contents of the list of sensors and the list of observations.
     * Currently the location is hard coded but is intended to be retrieved from the STA FeatureOfInteres (FOI) aswell.
     * Reason was that we ran out of time to edit the sensors data.
     * @param {*} sensors 
     * @param {*} observations 
     */
    function buildCZML(sensors, observations) {

        let arSensor = {
            'TMS-B01-R011': { lon: 9.173179434645018, lat: 48.78023467631227, height: 3 },
            'TMS-B01-R013': { lon: 9.173323528491817, lat: 48.78028009149803, height: 3 },
            'TMS-B01-R020': { lon: 9.17292039489995, lat: 48.780674195994656, height: 3 },
            'TMS-B01-R024': { lon: 9.172535417485694, lat: 48.78055265293503, height: 3 },
            'TMS-B01-R028': { lon: 9.172803497211259, lat: 48.780509390152794, height: 3 },
            'TMS-B01-R106': { lon: 9.172849401569167, lat: 48.78012850152515, height: 6 },
            'TMS-B01-R108': { lon: 9.173047156535324, lat: 48.780193331737074, height: 6 },
            'TMS-B01-R211': { lon: 9.173308692126207, lat: 48.78037143675138, height: 9 },
            'TMS-B01-R303': { lon: 9.172870728909714, lat: 48.78014412271331, height: 13 }
        }

        let minMax = [];

        // DEBUG
        // console.log('Obs: ', observations)
        // console.log('Obs: ', observations['TMS-B01-R011']);
        // console.log('Obs: ', observations['TMS-B01-R011'][0]);

        minMax[0] = observations['TMS-B01-R011'][0][3];
        minMax[1] = observations['TMS-B01-R011'][observations['TMS-B01-R011'].length - 2][3];

        // DEBUG
        // minMax[0] = sortedObs[0];
        // minMax[1] = sortedObs[sortedObs.length - 2];
        // console.log(minMax[0] + '/' + minMax[1]);
        // console.log(minMax[0]);

        let czml = [
            {
                "id": "document",
                "name": "CZML Geometries: Circles and Ellipses",
                "version": "1.0",
                "clock": {
                    "interval": minMax[0] + '/' + minMax[1],
                    "currentTime": minMax[0],
                    "multiplier": 3600
                }
            }
        ]

        for (var i = 0; i < sensors.length; i++) {
            let tempArray = [];
            for (var j = 0; j < observations[sensors[i].name].length; j++) {
                tempArray.push(observations[sensors[i].name][j][3], observations[sensors[i].name][j][2]);
            }

            czml.push({
                "id": sensors[i].name,
                "name": sensors[i].name,
                "position": {
                    "cartographicDegrees": [arSensor[sensors[i].name].lon, arSensor[sensors[i].name].lat, arSensor[sensors[i].name].height]
                },
                "ellipsoid": {
                    "radii": {
                        "cartesian": [0.5, 0.5, 0.5]
                    },
                    "fill": true,
                    "material": {
                        "solidColor": {
                            "color": {
                                "rgba": [255, 0, 0, 100]
                            }
                        }
                    },
                    // "outline": true,
                    // "outlineColor": {
                    //     "rgbaf": [0, 0, 0, 1]
                    // }
                },
                "properties": {
                    "constant_property": true,
                    "tempScale": {
                        "number": tempArray
                    }
                }
            });
        }

        return czml;
    }

    function clearSensors() {
        fetchedSensorList.length = 0;
        console.log('...cleared.');
    }

    init();

    return {
        sayHello: function () {
            return console.log('GssSetup: Hello!');
        },
        createSensors,
        insertObservations,
        getThings,
        getObservations,
        loadDatastreams,
        fetchedSensorList,
        fetchedObservationList,
        buildCZML,
        clearSensors
    }
})();