const obj2gltf = require('obj2gltf');
const options = {
    separateTextures: true // Don't embed textures in the converted glTF
}

// const args = process.argv.slice(2);
// console.log(args);

// === Customize ========================

let folder = '';
let inputFile = 'sensor.obj';
let outputFile = 'sensor2.gltf';

// === ==================================

const conPath = folder === '' ? '' : folder + '/'

obj2gltf('convert/' + inputFile, 'public/models/converted/' + conPath + outputFile, options)
    .then(function () {
        console.log('Converted model');
    });